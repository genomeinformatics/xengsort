# Changelog

### 2.0.8
* Fix Manifest

### 2.0.7
* Support NumPy 2.0.0
* Update README

### 2.0.6
* Internal deployment tooling update for faster release process


### 2.0.5
* Replace setup.py with pyproject.toml
* Update tests
* remove version.py and use metadata instead

### 2.0.4
* Fix missing imports.

### 2.0.3
* The new command `xengsort info` provides information about an existing hash and can output the data.

### 2.0.2
* Fix error using choices and jsonargparse
* Update README and Snakefile

### 2.0.1
* Support multiple fastq files as one sample.

### 2.0.0
* New classification method based on covered base pairs.
* Support compressed IO (gz (default), bzip2, xz).
* Use index as shared memory.

### 1.5.0
* Much faster index creation using parallelization via subtables.
* Use `.zarr` format instead of HDF5 format for storing the index.
* Note: Previous indexes are incompatible; please re-create all indexes!

### 1.1.0
* Fixed a bug/inconsistency that was introduced when the `hdf5` and `h5py` libraries changed their default behavior decoding strings vs. bytes.
  It could happen that the rcmode parameter of the stored hash table (how to deal with reverse complements) would be interpreted in a wrong manner; this could lead to inconsistent results.
  We strongly recommend to update to ensure that this bug does not affect your results.

### 1.0.3
* Change description and README because of `easy_install` deprecation

### 1.0.2
* Make `--out` a require parameter for `xengsort classify`.
* Improve the documentation, especially the troubleshooting section.

### 1.0.1
* Better error messages when erroneously giving gzipped FASTQ files to `xengsort classify` instead of uncompressed FASTQ files.
* Make `-n` a required parameter for xengsort index.

### 1.0.0
* Initial public release.
