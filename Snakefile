# Snakefile demonstrating the use of
# * xergsort index
# * xengsort classify
# using a human-captured mouse exome sample
# References and sample data will be downloaded from Ensembl / SRA, respectively.


# Define reference sequence:
# All human and mouse DNA and cDNA from Ensembl
RELEASE = "98"
ENSEMBL_HUMAN = f"ftp://ftp.ensembl.org/pub/release-{RELEASE}/fasta/homo_sapiens"
ENSEMBL_MOUSE = f"ftp://ftp.ensembl.org/pub/release-{RELEASE}/fasta/mus_musculus"

TOPLEVEL_HUMAN = "Homo_sapiens.GRCh38.dna.toplevel.fa.gz"
TOPLEVEL_HUMAN_DOWNLOAD = f"{ENSEMBL_HUMAN}/dna/{TOPLEVEL_HUMAN}"
CDNA_HUMAN = "Homo_sapiens.GRCh38.cdna.all.fa.gz"
CDNA_HUMAN_DOWNLOAD = f"{ENSEMBL_HUMAN}/cdna/{CDNA_HUMAN}"

TOPLEVEL_MOUSE = "Mus_musculus.GRCm38.dna.toplevel.fa.gz"
TOPLEVEL_MOUSE_DOWNLOAD = f"{ENSEMBL_MOUSE}/dna/{TOPLEVEL_MOUSE}"
CDNA_MOUSE = "Mus_musculus.GRCm38.cdna.all.fa.gz"
CDNA_MOUSE_DOWNLOAD = f"{ENSEMBL_MOUSE}/cdna/{CDNA_MOUSE}"


# Define the samples to process (SRA accession number)
# As an example, a single human-captured mouse exome is used.
# A whole list of sample accession numbers can be provided.
# Sample data is downloaded from SRA using the fasterq-dump wrapper.
SAMPLES = ["SRR9130497.1"]


# Define the final desired output files as input to rule "all"
rule all:
    input:
        "ref/xengsort-k25.info",
        "ref/xengsort-k25.hash",
        expand("results/{sample}-k25.log", sample=SAMPLES),
        expand("results/{sample}-k25-host.1.fq.gz", sample=SAMPLES),
        expand("results/{sample}-k25-host.2.fq.gz", sample=SAMPLES),



# A rule that demonstrates 'xengsort classify'
# (In principle, ALL output files should be listed, but all are created together,
#  so if we successfully get the -host.?.fq files, we get all of them.
#  And we do not need to process them any further.)
rule classify_sample:
    output:
        host1="results/{sample}-k{k}-host.1.fq.gz",
        host2="results/{sample}-k{k}-host.2.fq.gz",
        # other output files: -graft, -both, -neither, -ambiguous
    input:
        index_info="ref/xengsort-k{k}.info",
        index_hast="ref/xengsort-k{k}.hash",
        in1="raw/{sample}_1.fastq",
        in2="raw/{sample}_2.fastq",
    log:
         "results/{sample}-k{k}.log",
    benchmark:
        "results/{sample}-k{k}.benchmark"
    params:
        index=lambda wc: f"ref/xengsort-k{wc.k}",
        outprefix=lambda wc: f"results/{wc.sample}-k{wc.k}",
        chunksize=16.0,  # MB per chunk per thread
        #numactl="numactl -N 1 -m 1 ",
        prefetch=1,
        debug="-DD",  # debug level 2
    threads: 8
    shell:
        " xengsort {params.debug} classify --out {params.outprefix} --index {params.index} "
        " --threads {threads} --fastq {input.in1} --pairs {input.in2} "
        " --chunksize {params.chunksize} --prefetchlevel {params.prefetch} &> {log}"


# A rule that demonstrates how to build an index with xengsort
# Note that you need to give an estimate of the number of k-mers in all input files.
# For human and mouse DNA and cDNA, and k=25, this is approx. 4.5 billion,
# so this number is given as size parameter below.
rule build_index:
    input:
        dna_human=f"ref/{TOPLEVEL_HUMAN}",
        cdna_human=f"ref/{CDNA_HUMAN}",
        dna_mouse=f"ref/{TOPLEVEL_MOUSE}",
        cdna_mouse=f"ref/{CDNA_MOUSE}",
    output:
        "ref/xengsort-k{k}.info",
        "ref/xengsort-k{k}.hash"
    log:
        "ref/xengsort-k{k}.log",
    benchmark:
        "ref/xengsort-k{k}.benchmark"
    params:
        index=lambda wc: f"ref/xengsort-k{wc.k}",
        size=4496607845,  # size only correct for k=25!
        fill=0.88,  # total space in table is size/fill
        subtables=9,
        weakthreads=11,
        bucketsize=4,
        hashfuncs="",
        debug="-DD",  # debug level 2
        #numactl="numactl -N 1 -m 1",
    threads: 11
    shell:
        "xengsort {params.debug} index  --index {params.index} "
        " -G {input.cdna_human} {input.dna_human} "
        " -H {input.cdna_mouse} {input.dna_mouse} "
        " -k {wildcards.k} -n {params.size} "
        " -p {params.bucketsize} --fill {params.fill} -W {params.weakthreads} &> {log}"


# This rule simply downloads all reference sequences (FASTA) using wget.
# This is not very elegant in terms of error recovery but should work.
rule download_refs:
    output:
        f"ref/{TOPLEVEL_HUMAN}",
        f"ref/{TOPLEVEL_MOUSE}",
        f"ref/{CDNA_HUMAN}",
        f"ref/{CDNA_MOUSE}",
    shell:
        """
        wget {TOPLEVEL_HUMAN_DOWNLOAD} -P ref/
        wget {TOPLEVEL_MOUSE_DOWNLOAD} -P ref/
        wget {CDNA_HUMAN_DOWNLOAD} -P ref/
        wget {CDNA_MOUSE_DOWNLOAD} -P ref/
        """


# This rule downloads paired-end FASTQ sequences from an SRA accession.
# It uses a Snakemake wrapper, and it requires sra-tools to be installed.
# This is automatically done if Snakemake is invoked with  the --use-conda option.
rule get_fastq_pe:
    output:
        # the wildcard name must be accession, pointing to an SRA number
        "raw/{accession}_1.fastq",
        "raw/{accession}_2.fastq"
    params:
        # optional extra arguments
    threads: 6  # defaults to 6
    wrapper:
        "0.70.0/bio/sra-tools/fasterq-dump"
