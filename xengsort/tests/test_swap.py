import numpy as np
from fastcash.xengsort.xengsort_weak import compile_swap_kmer_parts

partsizes = [8,9,8]
valuebits = 4
k = sum(partsizes)
swap_right, swap_back = compile_swap_kmer_parts(partsizes, valuebits)
high = 4**k * 2**valuebits
vmask = (2**valuebits - 1)
pshift = 2 * sum(partsizes[1:]) + valuebits
pmask = (4**partsizes[0]- 1) << pshift
original = np.random.randint(high, size=10_000_000, dtype=np.uint64)
codes = np.empty_like(original)

print("random codes:", original[:3])
print("right and back")
codes[:] = original[:]
swap_right(codes)
assert np.all((codes & vmask) == (original & vmask))
assert np.all((codes & pmask) == (original & pmask))
swap_back(codes)
assert np.all(codes==original)

print("back and right")
codes[:] = original[:]
swap_back(codes)
assert np.all((codes & vmask) == (original & vmask))
assert np.all((codes & pmask) == (original & pmask))
swap_right(codes)
assert np.all(codes==original)
