import numpy as np
from numba import njit

from fastcash.lowlevel.bitarray import bitarray
from fastcash.lowlevel.numbautils import numba_return_type_str
from fastcash.lowlevel.llvm import compile_popcount


def test_empty():
    b = bitarray(1025)
    assert b.capacity == 1024 + 64
    assert b.capacity_bytes == (1024 + 64) // 8
    assert b.capacity_ints == (1024 + 64) // 64
    a = b.array
    for i in range(1025):
        assert b.get(a, i) == 0
    assert numba_return_type_str(b.get) == "uint64"


def test_full():
    b = bitarray(514)
    assert b.capacity == 512 + 64
    assert b.capacity_bytes == (512 + 64) // 8
    assert b.capacity_ints == (512 + 64) // 64
    a = b.array
    for i in range(513):
        b.set(a, i, 1)
        m = min(i + 1, 64)
        assert b.get(a, i - m + 1, m) == 2**m - 1
        m = min(513 - i, 64)
        assert b.get(a, i + 1, m) == 0  # i+1 < 514
    assert numba_return_type_str(b.set) == "none"
    assert numba_return_type_str(b.get) == "uint64"


def test_61():
    n = 2051
    b = bitarray(n)
    a = b.array
    m = 61
    value = 2**61 - 1 - 2**54
    for i in range(n - m + 1):
        pi = max(0, i - 64)
        pm = i - pi
        sm = min(n - i - m + 1, 64)
        print(i, "|", pi, pm, "|", i + m, sm)
        b.set(a, i, value, m)
        assert b.get(a, i, m) == value
        assert b.get(a, pi, pm) == 0
        assert b.get(a, i + m, sm) == 0
        b.set(a, i, 0, m)
        assert b.get(a, i, m) == 0
    assert numba_return_type_str(b.set) == "none"
    assert numba_return_type_str(b.get) == "uint64"


def test_61_4():
    n = 2051
    b = bitarray(n)
    a = b.array
    m = 61
    value = 2**61 - 1 - 2**54
    i = 4
    b.set(a, i, value, m)
    assert b.get(a, i, m) == value
    assert numba_return_type_str(b.set) == "none"
    assert numba_return_type_str(b.get) == "uint64"


popcount = compile_popcount("uint64")


@njit
def do_popcount(v, i):
    return int(popcount(v[i]))


def test_512_bits_1():
    n = 512
    b = bitarray(n)
    a = b.array
    v = np.array([11091872975622749660, 10591287472622885615, 13757223617689515903,
 18443911181698588535, 18446698117504944027, 16694800368012885141,
 18283400626285821271, 18043953136663723006], dtype=np.uint64)
    a[:] = v
    p1 = b.popcount(a)
    p2 = b.popcount(a, 0, 512)
    p3 = sum(int(b.get(a, i)) for i in range(512))
    p4 = sum(int(b.getquick(a, i)) for i in range(512))
    p5 = sum(do_popcount(v, i) for i in range(v.size))
    assert p1 == p2
    assert p3 == p4
    assert p2 == p3
    assert p4 == p5


def test_512_bits_2():
    n = 512
    b = bitarray(n)
    a = b.array
    v = np.array([1227481906065591892, 10559965740272272721, 453708559681090156,
  9530838012487783233, 2410319860021949638, 5175164166933101645,
   637660840810724251, 8929092314088281459], dtype=np.uint64)
    a[:] = v
    p1 = b.popcount(a)
    p2 = b.popcount(a, 0, 512)
    p3 = sum(int(b.get(a, i)) for i in range(512))
    p4 = sum(int(b.getquick(a, i)) for i in range(512))
    p5 = sum(do_popcount(v, i) for i in range(v.size))
    assert p1 == p2
    assert p3 == p4
    assert p2 == p3
    assert p4 == p5
