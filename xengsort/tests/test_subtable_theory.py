from math import ceil, sqrt
import numpy as np
from scipy.stats import norm


def compute_max_load(nobjects, subtables, average_load=0.9, certainties=None):
    if nobjects < 1_000_000:
        raise ValueError("approximations only valid for large samples")
    if subtables > 1025:
        raise ValueError("so many subtables don't make sense")
    if not (0 < average_load < 1):
        raise ValueError("average load must be in ]0,1[")
    if certainties is None:
        certainties = [0.9, 0.95, 0.99, 0.999, 0.9999, 1.0]
    n, p = nobjects, 1.0/subtables
    E = n * p
    V = E * (1-p)
    std = sqrt(V)  # standard deviation
    ##print(f"{nobjects} objects, {subtables} subtables: {E:.1f} +- {std:.1f} per subtable")
    ##print(f"Average load: {average_load}")

    # We approximate the distribution in any one of the subtables by a
    # Gaussian N(E, V) distribution.
    # We take the maximum over subtables realizations.
    # So let S:=subtables, and X_1, ..., X_S have N(E,V) distribution.
    # What is the distribution of M := max(X_1,...,X_S)?
    # Prob[M<=m] = product(i=1...S, Prob[X_i<=m]) = Prob[X_1<=m]**S
    # The load corresponding to m is m/E.
    # This approximation is not correct because the X_i are in fact dependent,
    # as their sum is exactly n. So we must have M >= E.
    # We can apply a small correction to enforce this with probability 1.
    high = int(ceil(E/average_load))
    ms = np.linspace(E, high, 4_000_001)
    cdf = (norm.cdf(ms, loc=E, scale=std))**subtables
    pdf = 0.0  # TODO: pmf for E on Binomial(n,p)
    error = cdf[0] - pdf**subtables
    if error > 1e-8:
        ##print(f"Mass error: {cdf[0]}, correcting...")
        cdf = (cdf - error) / (1-error)
    loads = (average_load/E) * ms
    good = int(np.amin(np.flatnonzero(cdf >= certainty)))
    return loads[good]


if __name__ == "__main__":
    nobjects = 3_102_953_939
    #nobjects = 3_102_954
    load = 0.9
    certainty = 0.5
    for subtables in range(3, 18, 2):
        max_load = compute_max_load(nobjects, subtables, average_load=load, certainties=[certainty])
        print(f"{subtables:02d} subtables: w/prob >= {certainty}, max load is <= {max_load:.5f}")