from pprint import pprint
from fastcash.h5utils import save_to_h5group, load_from_h5group


def test_save_load():
    rcmode = "max"
    info = dict(rcmode=rcmode, k=17, somedict=dict(foo=b'foo', bar="bar"))
    save_to_h5group("test_h5_encoding.h5", "info", **info)
    newinfo = load_from_h5group("test_h5_encoding.h5", "info")
    pprint(info)
    pprint(newinfo)
    assert info == newinfo


if __name__ == "__main__":
    test_save_load()
