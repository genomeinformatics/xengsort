from fastcash.filter_b3c import build_filter
from math import ceil
from numba import njit

size = 1 # filter size in GB
k = 25
universe = 4**k
nsubfilter = 9


def test_creater_filter():
    bf = build_filter(k, universe, size, nsubfilter, "random")
    t_mem_bytes = ceil(((size * 1024 * 1024 * 1024) // nsubfilter) / 512) * 512 * nsubfilter
    assert bf.mem_bytes == t_mem_bytes


def test_set_get():
    bf = build_filter(k, universe, size, nsubfilter, "random")

    key = 16034544569
    bf.store_new(bf.filter_array, key)
    bf.store_new(bf.filter_array, key)
    bf.store_new(bf.filter_array, key)
    assert bf.get_value(bf.filter_array, key)

    get_value_at = bf.private.get_value_at
    b = bf.filterbits

    @njit()
    def count(a):
        c = 0
        for i in range(b):
            c += get_value_at(a, i)
        return c

    count_ones = count(bf.filter_array)
    print(count_ones)
    assert count_ones == 3