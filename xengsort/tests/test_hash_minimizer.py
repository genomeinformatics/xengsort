import pytest
import numpy as np
from numba import njit, uint8, uint64
from fastcash.hash_minimizer import build_hash
from fastcash.superkmers import compile_superkmer_processor
from fastcash.mask import create_mask
from fastcash.dnaencode import dna_to_2bits, compile_revcomp_and_canonical_code
from collections import defaultdict
import itertools


def test_build_hash():
    k = 25
    m = 17
    rcmode = "max"
    n = 5_000_000
    subtables = 9
    bucketsize = 4
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 255
    update_value = lambda x: x+1
    build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)

def test_setter_getter():
    k = 25
    m = 21
    rcmode = "max"
    n = 2_000_000
    subtables = 7
    bucketsize = 6
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16
    update_value = lambda x: x+1
    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    get_value = h.private.get_value_at
    set_value = h.private.set_value_at
    set_m = h.private.set_minimizer_at
    set_l = h.private.set_leftpart_at
    set_r = h.private.set_rightpart_at
    get_m = h.private.get_minimizer_at
    get_l = h.private.get_leftpart_at
    get_r = h.private.get_rightpart_at

    subtable = 3
    bucket = 2
    slot = 3
    kmer = 1 #second kmer in the super kmer
    value = 10

    minimizer = int("0101000100111100101010010",2) # we only have 25 bits if we use double quotienting
    left = int("10110100",2) #GTCA
    leftsize = 4
    right = int("11100011",2) #TGAT
    rightsize = 4
    set_m(ht,subtable,bucket,slot,minimizer)
    set_r(ht,subtable,bucket,slot, right, rightsize)
    set_l(ht,subtable,bucket,slot, left, leftsize)
    set_value(ht,subtable,bucket,slot,kmer,value)
    assert get_value(ht,subtable,bucket,slot,kmer) == value
    rleft, rleftsize = get_l(ht,subtable,bucket,slot)
    assert leftsize == rleftsize
    assert left == rleft
    assert get_m(ht,subtable,bucket,slot) == minimizer
    rright, rrightsize = get_r(ht,subtable,bucket,slot)
    assert rightsize == rrightsize
    assert rright == right

    left = int("101101",2) #GTC
    leftsize = 3
    right = int("100011",2) #GAT
    rightsize = 3
    bucket = 4

    set_m(ht,subtable,bucket,slot,minimizer)
    set_r(ht,subtable,bucket,slot, right, rightsize)
    set_l(ht,subtable,bucket,slot, left, leftsize)
    set_value(ht,subtable,bucket,slot,kmer,value)

    assert get_value(ht,subtable,bucket,slot,kmer) == value
    rleft, rleftsize = get_l(ht,subtable,bucket,slot)
    assert leftsize == rleftsize
    assert left == rleft
    assert get_m(ht,subtable,bucket,slot) == minimizer
    rright, rrightsize = get_r(ht,subtable,bucket,slot)
    print(rrightsize)
    assert rightsize == rrightsize
    assert rright == right

def test_set_get_signature_at():
    k = 25
    m = 21
    rcmode = "max"
    n = 2_000_000
    subtables = 7
    bucketsize = 6
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16
    update_value = lambda x: x+1
    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    set_sig_at = h.private.set_signature_at
    get_sig_at = h.private.get_signature_at

    # we only have 25 bits if we use double quotienting + 2 choice bits
    sig = int("100101000100111100101010010",2)
    subtable = 3
    bucket = 2
    slot = 3

    set_sig_at(ht, subtable, bucket, slot, sig)
    assert sig == get_sig_at(ht, subtable, bucket, slot)

def test_set_get_item_at():
    k = 25
    m = 21
    rcmode = "max"
    n = 2_000_000
    subtables = 7
    bucketsize = 6
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16
    update_value = lambda x: x+1
    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    set_item = h.private.set_item_at
    get_item = h.private.get_item_at
    get_sig_at = h.private.get_signature_at

    subtable = 3
    bucket = 2
    slot = 3
    values = np.array([10,89,12])
    rvalues = np.zeros(k-m+1, dtype=np.uint64)

    sig = int("100101000100111100101010010",2) # we only have 25 bits if we use double quotienting + 2 choice bits
    left = int("10110100",2) #GTCA
    leftsize = 4
    right = int("1110",2) #TGAT
    rightsize = 2

    set_item(ht, subtable, bucket, slot, sig, left, leftsize, right, rightsize, values)
    rsig, rleftsize, rleftpart, rrightsize, rrightpart = get_item(ht, subtable, bucket, slot, rvalues)
    assert sig == rsig
    assert leftsize == rleftsize
    assert left == rleftpart
    assert rightsize == rrightsize
    assert right == rrightpart
    assert (values == rvalues[:3]).all()

    sig2 = int("100101000010100101001010010",2) # we only have 25 bits if we use double quotienting + 2 choice bits
    left2 = int("10110100",2) #GTCA
    leftsize2 = 4
    right2 = int("1110",2) #TGAT
    rightsize2 = 2

    set_item(ht, subtable, bucket, slot-1, sig2, left2, leftsize2, right2, rightsize2, values)
    set_item(ht, subtable, bucket, slot+1, sig2, left2, leftsize2, right2, rightsize2, values)
    rsig, rleftsize, rleftpart, rrightsize, rrightpart = get_item(ht, subtable, bucket, slot, rvalues)
    assert sig == rsig
    assert get_sig_at(ht, subtable, bucket, slot) == sig
    assert leftsize == rleftsize
    assert left == rleftpart
    assert rightsize == rrightsize
    assert right == rrightpart
    assert (values == rvalues[:3]).all()

    rsig2, rleftsize2, rleftpart2, rrightsize2, rrightpart2 = get_item(ht, subtable, bucket, slot-1, rvalues)
    rsig3, rleftsize3, rleftpart3, rrightsize3, rrightpart3 = get_item(ht, subtable, bucket, slot+1, rvalues)
    assert sig2 == rsig2 == rsig3
    assert get_sig_at(ht, subtable, bucket, slot-1) == sig2
    assert get_sig_at(ht, subtable, bucket, slot+1) == sig2
    assert leftsize2 == rleftsize2 == rleftsize3
    assert left2 == rleftpart2 == rleftpart3
    assert rightsize2 == rrightsize2 == rrightsize3
    assert right2 == rrightpart2 == rrightpart3

def test_right_extend():
    mask = create_mask(25)
    k = mask.k
    m = 21


    # Generate rnd seq
    size = 5_000_000
    gc = 0.4
    at = 1-gc
    # seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)
    np.random.seed(42)
    seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)

    # Create hash table
    rcmode = "max"
    n = 2_000_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    bt = h.backuptable.hashtable
    ht = h.hashtable
    update = h.update
    org_values = np.ones(k-m+1, dtype=np.uint64)
    for i in range(len(org_values)):
        org_values[i]=i+1

    ht1 = ht.copy()
    bt1 = bt.copy()
    lock = h.backuplock
    stored_values = np.zeros(k-m+1, dtype=np.uint64)


    new_values = org_values.copy()
    minimizer = int("0101000100111100101010010",2)
    update(ht1, bt1, lock, 0, minimizer, int("10110100",2), 4, int("11100011",2), 4, new_values)
    update(ht1, bt1, lock, 0, minimizer, int("1000",2), 2, int("1110",2), 2, new_values[4-2:4-2+m+2+2-k+1])
    update(ht1, bt1, lock, 0, minimizer, int("11000100",2), 4, 0, 0, new_values[0:1])
    new_minimizer, new_leftsize1, new_leftpart1, new_rightsize1, new_rightpart1 = h.private.get_item_at(ht1, 0, 52881, 0, stored_values)
    assert (stored_values == [1,2,3,4,5]).all()
    stored_values[:] = 0
    new_minimizer, new_leftsize2, new_leftpart2, new_rightsize2, new_rightpart2 = h.private.get_item_at(ht1, 0, 52881, 1, stored_values)
    assert (stored_values == [3,0,0,0,0]).all()
    stored_values[:] = 0
    new_minimizer, new_leftsize3, new_leftpart3, new_rightsize3, new_rightpart3 = h.private.get_item_at(ht1, 0, 52881, 2, stored_values)
    assert (stored_values == [1,0,0,0,0]).all()
    stored_values[:] = 0

    assert new_leftsize1 == 4
    assert new_leftsize2 == 2
    assert new_leftsize3 == 4
    assert new_rightsize1 == 4
    assert new_rightsize2 == 2
    assert new_rightsize3 == 0

    assert new_leftpart1 == int("10110100",2)
    assert new_leftpart2 == int("1000",2)
    assert new_leftpart3 == int("11000100",2)
    assert new_rightpart1 == int("11100011",2)
    assert new_rightpart2 == int("1110",2)
    assert new_rightpart3 == 0


    print()
    print()
    print("--------------")
    update(ht1, bt1, lock, 0, minimizer, int("11000100",2), 4, int("11101100",2), 4, new_values)

    new_minimizer, new_leftsize1, new_leftpart1, new_rightsize1, new_rightpart1 = h.private.get_item_at(ht1, 0, 52881, 0, stored_values)
    assert (stored_values == [1,2,6,4,5]).all()
    stored_values[:] = 0
    new_minimizer, new_leftsize2, new_leftpart2, new_rightsize2, new_rightpart2 = h.private.get_item_at(ht1, 0, 52881, 1, stored_values)
    assert (stored_values == [3,4,5,0,0]).all()
    stored_values[:] = 0
    new_minimizer, new_leftsize3, new_leftpart3, new_rightsize3, new_rightpart3 = h.private.get_item_at(ht1, 0, 52881, 2, stored_values)
    assert (stored_values == [2,2,0,0,0]).all()
    stored_values[:] = 0
    assert new_leftsize1 == 4
    assert new_leftsize2 == 2
    assert new_leftsize3 == 4
    assert new_rightsize1 == 4
    assert new_rightsize2 == 4
    assert new_rightsize3 == 1

    assert new_leftpart1 == int("10110100",2)
    assert new_leftpart2 == int("1000",2)
    assert new_leftpart3 == int("11000100",2)
    assert new_rightpart1 == int("11100011",2)
    assert new_rightpart2 == int("11101100",2)
    assert new_rightpart3 == int("11",2)


def test_left_extend():
    mask = create_mask(25)
    k = mask.k
    m = 21


    # Generate rnd seq
    size = 5_000_000
    gc = 0.4
    at = 1-gc
    # seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)
    np.random.seed(42)
    seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)

    # Create hash table
    rcmode = "max"
    n = 2_000_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16
    stored_values = np.zeros(k-m+1, dtype=np.uint64)

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    bt = h.backuptable.hashtable
    ht = h.hashtable
    update = h.update
    org_values = np.ones(k-m+1, dtype=np.uint64)
    for i in range(len(org_values)):
        org_values[i]=i+1

    ht1 = ht.copy()
    bt1 = bt.copy()
    lock = h.backuplock


    new_values = org_values.copy()
    minimizer = int("0101000100111100101010010",2)
    update(ht1, bt1, lock, 0, minimizer, int("10110100",2), 4, int("11100011",2), 4, new_values)
    update(ht1, bt1, lock, 0, minimizer, int("0100",2), 2, int("1101",2), 2, new_values[4-2:4-2+m+2+2-k+1])
    update(ht1, bt1, lock, 0, minimizer, 0, 0, int("11100100",2), 4, new_values[-1:])
    new_minimizer, new_leftsize1, new_leftpart1, new_rightsize1, new_rightpart1 = h.private.get_item_at(ht1, 0, 52881, 0, stored_values)
    assert (stored_values == [1,2,3,4,5]).all()
    stored_values[:] = 0
    new_minimizer, new_leftsize2, new_leftpart2, new_rightsize2, new_rightpart2 = h.private.get_item_at(ht1, 0, 52881, 1, stored_values)
    assert (stored_values == [3,0,0,0,0]).all()
    stored_values[:] = 0
    new_minimizer, new_leftsize3, new_leftpart3, new_rightsize3, new_rightpart3 = h.private.get_item_at(ht1, 0, 52881, 2, stored_values)
    assert (stored_values == [5,0,0,0,0]).all()
    stored_values[:] = 0
    assert new_leftsize1 == 4
    assert new_leftsize2 == 2
    assert new_leftsize3 == 0
    assert new_rightsize1 == 4
    assert new_rightsize2 == 2
    assert new_rightsize3 == 4

    assert new_leftpart1 == int("10110100",2)
    assert new_leftpart2 == int("0100",2)
    assert new_leftpart3 == 0
    assert new_rightpart1 == int("11100011",2)
    assert new_rightpart2 == int("1101",2)
    assert new_rightpart3 == int("11100100",2)

    # assert len(stored_values1) == 5
    # assert len(stored_values2) == 1
    # assert len(stored_values3) == 1
    # assert (stored_values1 == [1,2,3,4,5]).all()
    # assert (stored_values2 == [3]).all()
    # assert (stored_values3 == [5]).all()

    print()
    print()
    print("--------------")
    update(ht1, bt1, lock, 0, minimizer, int("11010100",2), 4, int("11100100",2), 4, new_values)

    new_minimizer, new_leftsize1, new_leftpart1, new_rightsize1, new_rightpart1 = h.private.get_item_at(ht1, 0, 52881, 0, stored_values)
    assert (stored_values == [1,2,6,4,5]).all()
    stored_values[:] = 0
    new_minimizer, new_leftsize2, new_leftpart2, new_rightsize2, new_rightpart2 = h.private.get_item_at(ht1, 0, 52881, 1, stored_values)
    assert (stored_values == [1,2,3,0,0]).all()
    stored_values[:] = 0
    new_minimizer, new_leftsize3, new_leftpart3, new_rightsize3, new_rightpart3 = h.private.get_item_at(ht1, 0, 52881, 2, stored_values)
    assert (stored_values == [4,10,0,0,0]).all()
    assert new_leftsize1 == 4
    assert new_leftsize2 == 4
    assert new_leftsize3 == 1
    assert new_rightsize1 == 4
    assert new_rightsize2 == 2
    assert new_rightsize3 == 4

    assert new_leftpart1 == int("10110100",2)
    assert new_leftpart2 == int("11010100",2)
    assert new_leftpart3 == 0
    assert new_rightpart1 == int("11100011",2)
    assert new_rightpart2 == int("1101",2)
    assert new_rightpart3 == int("11100100",2)

    # assert len(stored_values1) == 5
    # assert len(stored_values2) == 3
    # assert len(stored_values3) == 2
    # assert (stored_values1 == [1,2,6,4,5]).all()
    # assert (stored_values2 == [1,2,3]).all()
    # assert (stored_values3 == [4,10]).all()

def test_update():
    mask = create_mask(25)
    k = mask.k
    m = 21

    # Generate rnd seq
    size = 5_000_000
    gc = 0.4
    at = 1-gc
    # seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)
    np.random.seed(42)
    seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)

    # Create hash table
    rcmode = "max"
    n = 2_000_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16
    stored_values = np.zeros(k-m+1, dtype=np.uint64)

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    bt = h.backuptable.hashtable
    ht = h.hashtable
    update = h.update
    org_values = np.ones(k-m+1, dtype=np.uint64)
    for i in range(len(org_values)):
        org_values[i]=i+1

    ht1 = ht.copy()
    bt1 = bt.copy()
    lock = h.backuplock

    #insert and update super-k-mer
    print("1:")
    new_values = org_values.copy()
    minimizer = int("010100010011110010101001001010001001111001",2)
    left = int("110100",2) #TCA
    leftsize = 3
    right = int("111000",2) #TGA
    rightsize = 3
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m+leftsize+rightsize-k+1])
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 52881, 0, stored_values)
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == leftsize
    assert new_leftpart == left
    assert new_rightsize == rightsize
    assert new_rightpart == right
    assert (stored_values == [2,3,4,0,0]).all()

    # update existing super-k-mer with a smaller one
    print("2:")
    new_values = org_values.copy()
    left = int("0100",2) #CA
    leftsize = 2
    right = int("1110",2) #TG
    rightsize = 2
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    # stored super-k-mer is bigger
    assert new_leftsize == 3
    assert new_leftpart == int("110100",2) #TCA
    assert new_rightsize == 3
    assert new_rightpart == int("111000",2) #TGA
    assert (stored_values == [2,6,4,0,0]).all()


    print("3:")
    # extend existing super-k-mer left
    new_values = org_values.copy()
    left = int("10110100",2) #GTCA
    leftsize = 4
    right = int("1110",2) #TG
    rightsize = 2
    # print(f"{new_values[:m-k+1+leftsize+rightsize]=}")
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == leftsize
    assert new_leftpart == left
    assert new_rightsize == 3
    assert new_rightpart == int("111000",2) #TGA
    assert (stored_values == [1,4,9,4,0]).all()

    print("4:")
    # extend existing super-k-mer right
    new_values = org_values.copy()
    left = int("10110100",2) #GTCA
    leftsize = 4
    right = int("11100011",2) #TGAT
    rightsize = 4
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])
    _, _, _, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == leftsize
    assert new_leftpart == left
    assert new_rightsize == rightsize
    assert new_rightpart == right
    assert (stored_values == [2,6,12,8,5]).all()

    print("5:")
    # update existing super-k-mer with a smaller one
    new_values = org_values.copy()
    left = int("0100",2) #CA
    leftsize = 2
    right = int("1110",2) #TG
    rightsize = 2
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("10110100",2) #GTCA
    assert new_rightsize == 4
    assert new_rightpart == int("11100011",2) #TGAT
    assert (stored_values == [2,6,15,8,5]).all()

    print("6:")
    # update existing super-k-mer with a smaller one (first/most left k-mer)
    new_values = org_values.copy()
    left = int("10110100",2) #GTCA
    leftsize = 4
    right = 0
    rightsize = 0
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("10110100",2) #GTCA
    assert new_rightsize == 4
    assert new_rightpart == int("11100011",2) #TGA
    assert (stored_values == [3,6,15,8,5]).all()

    print("7:")
    # update existing super-k-mer with a smaller one (last/most right k-mer)
    new_values = org_values.copy()
    left = 0
    leftsize = 0
    right = int("11100011",2) #TGAT
    rightsize = 4
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("10110100",2) #GTCA
    assert new_rightsize == 4
    assert new_rightpart == int("11100011",2) #TGA
    assert (stored_values == [3,6,15,8,10]).all()

    print("8:")
    # same minimizer, different left and right parts
    new_values = org_values.copy()
    left = int("10110111",2) #GTCT
    leftsize = 4
    right = int("00100011",2) #AGAT
    rightsize = 4
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])
    # check first super-k-mer
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("10110100",2) #GTCA
    assert new_rightsize == 4
    assert new_rightpart == int("11100011",2) #TGA
    assert (stored_values == [3,6,15,8,10]).all()
    assert h.private.get_occbits_at(ht1, 0, 48672,0) == 2
    # check new inserted super-k-mer
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 1, stored_values)
    assert new_leftsize == leftsize
    assert new_leftpart == left
    assert new_rightsize == rightsize
    assert new_rightpart == right
    assert (stored_values == [1,2,3,4,5]).all()
    assert h.private.get_occbits_at(ht1, 0, 48672,1) == 2

    print("9:")
    # update middel k-mer and insert new left part
    new_values = org_values.copy()
    left = int("11000100",2) #TACA
    leftsize = 4
    right = int("1110",2) #TG
    rightsize = 2
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])

    #check first super-kmer
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("10110100",2) #GTCA
    assert new_rightsize == 4
    assert new_rightpart == int("11100011",2) #TGA
    assert (stored_values == [3,6,18,8,10]).all()
    assert h.private.get_occbits_at(ht1, 0, 48672,0) == 3

    #check second super-k-mer
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 1, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("10110111",2) #GTCT
    assert new_rightsize == 4
    assert new_rightpart == int("00100011",2) #AGAT
    assert (stored_values == [1,2,3,4,5]).all()
    assert h.private.get_occbits_at(ht1, 0, 48672,1) == 3

    # check new inserted super-k-mer
    stored_values[:] = 0
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 2, stored_values)
    assert new_leftsize == leftsize
    assert new_leftpart == left
    assert new_rightsize == 1
    assert new_rightpart == int("11",2) #T
    assert (stored_values == [1,2,0,0,0]).all()
    assert h.private.get_occbits_at(ht1, 0, 48672,1) == 3

    print("10:")
    revcomp, ccode = compile_revcomp_and_canonical_code(k, rcmode)
    # Insert super-k-mer that needs to be stored in the backup table
    new_values = org_values.copy()
    left = int("11000101",2) #TACC
    leftsize = 4
    right = int("01101100",2) #CGTA
    rightsize = 4
    update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m-k+leftsize+rightsize+1])

    #check first super-kmer
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 0, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("10110100",2) #GTCA
    assert new_rightsize == 4
    assert new_rightpart == int("11100011",2) #TGA
    assert (stored_values == [3,6,18,8,10]).all()
    assert h.private.get_occbits_at(ht1, 0, 48672,0) == 3
    assert h.private.get_backupflag_at(ht1, 0, 48672,0) == 1

    #check second super-k-mer
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 1, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("10110111",2) #GTCT
    assert new_rightsize == 4
    assert new_rightpart == int("00100011",2) #AGAT
    assert (stored_values == [1,2,3,4,5]).all()
    assert h.private.get_occbits_at(ht1, 0, 48672,1) == 3
    assert h.private.get_backupflag_at(ht1, 0, 48672,1) == 1

    # check new inserted super-k-mer
    stored_values[:] = 0
    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht1, 0, 48672, 2, stored_values)
    assert new_leftsize == 4
    assert new_leftpart == int("11000100",2) #TACA
    assert new_rightsize == 1
    assert new_rightpart == int("11",2) #T
    assert (stored_values == [1,2,0,0,0]).all()
    assert h.private.get_occbits_at(ht1, 0, 48672,1) == 3
    assert h.private.get_backupflag_at(ht1, 0, 48672,1) == 1

    get_value = h.backuptable.get_value
    superkmer = left << 50 | (minimizer << 8) | right
    mask = 4**k -1
    check_values = new_values[::-1]
    for i in range(5):
        kmer = (superkmer >> (2*i)) & mask
        assert get_value(bt1, ccode(kmer)) == check_values[i]

def test_repetitions():
    mask = create_mask(25, m=17)
    k = mask.k
    m = mask.m

    # Create hash table
    rcmode = "max"
    n = 2_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16
    stored_values = np.zeros(k-m+1, dtype=np.uint64)

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    bt = h.backuptable.hashtable
    lock = h.backuplock
    update = h.update
    values = np.ones(k-m+1, dtype=np.uint64)

    seq = dna_to_2bits(b"TGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGT")
    size = len(seq)

    @njit
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht1, bt1, lock, values):
        values = values[:leftsize + m + rightsize - k + 1]
        update(ht1, bt1, lock, 0, minimizer, leftpart, leftsize, rightpart, rightsize, values)
        return False

    kmers = compile_superkmer_processor(mask, _func)

    kmers(seq, 0 , len(seq), ht, bt, lock, np.ones(k-m+1, dtype=np.uint64))
    minimizer, leftsize, leftpart, rightsize, rightpart = h.private.get_item_at(ht, 0, 41, 0, stored_values)
    assert minimizer == 307632759
    assert leftpart == int("3",4)
    assert leftsize == 1
    assert rightpart == int("22222222",4)
    assert rightsize == 8
    print(stored_values)
    print([1, size-k-1,0,0,0,0,0,0,0])
    print(stored_values == [1, size-k-1,0,0,0,0,0,0,0])
    assert (stored_values == [1, size-k-1,0,0,0,0,0,0,0]).all()


    stored_values[:]=0
    minimizer, leftsize, leftpart, rightsize, rightpart = h.private.get_item_at(ht, 0, 41, 1, stored_values)
    assert minimizer == 307632759
    assert leftpart == 0
    assert leftsize == 0
    assert rightpart == int("22222223",4)
    assert rightsize == 8
    assert (stored_values == [1,0,0,0,0,0,0,0,0]).all()

def test_is_slot_empty():
    # Create hash table
    k = 25
    m = 21
    rcmode = "max"
    n = 2_000_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16
    update_value = lambda x: x+1
    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    slot_empty = h.private.is_slot_empty_at
    set_sig = h.private.set_signature_at

    sig = int("110101000100111100101010010",2)

    assert slot_empty(ht, 0, 3, 3)
    set_sig(ht, 0, 3, 3, sig)
    assert not slot_empty(ht, 0, 3, 3)

# def test_update_repetitive():
#     mask = create_mask(25)
#     k = mask.k
#     m = 21

#     # Generate rnd seq
#     size = 5_000_000
#     gc = 0.4
#     at = 1-gc
#     # seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)
#     np.random.seed(42)
#     seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)

#     # Create hash table
#     rcmode = "max"
#     n = 2_000_000
#     subtables = 1
#     bucketsize = 16
#     hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
#     nvalues = 2**16

#     @njit
#     def update_value(old, new):
#         return uint64(min(old + new, nvalues-1))

#     h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
#     bt = h.backuptable.hashtable
#     ht = h.hashtable
#     update = h.update
#     org_values = np.ones(k-m+1, dtype=np.uint64)
#     for i in range(len(org_values)):
#         org_values[i]=i+1

#     ht1 = ht.copy()
#     bt1 = bt.copy()
#     lock = h.backuplock

#     #insert and update super-k-mer
#     print("1:")
#     new_values = org_values.copy()
#     minimizer = 0 # All As
#     left = 0 # AAAA
#     leftsize = 4
#     right = 0 # AAAA
#     rightsize = 4
#     update(ht1, bt1, lock, 0, minimizer, left, leftsize, right, rightsize, new_values[4-leftsize:4-leftsize+m+leftsize+rightsize-k+1])
#     new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart, stored_values = h.private.get_item_at(ht1, 0, 0, 0)
#     with pytest.raises(Exception):
#         assert new_leftsize == leftsize
#         assert new_leftpart == left
#         assert new_rightsize == rightsize
#         assert new_rightpart == right
#         assert (stored_values == new_values).all()

def test_all_kmers():
    mask = create_mask(5, m=3)
    k = mask.k
    m = mask.m

    # Create hash table
    rcmode = "max"
    n = 2_000_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    bt = h.backuptable.hashtable
    ht = h.hashtable
    update = h.update
    org_values = np.ones(k-m+1, dtype=np.uint64)
    for i in range(len(org_values)):
        org_values[i]=i+1

    lock = h.backuplock
    update = h.update
    kmer_mask = 4**k-1
    inserted = defaultdict(lambda: False)
    seq = ""
    lseq = ""
    rseq = ""
    inner_seq = "A"*(k-1)
    ckmer = "A"*(k-1)
    while True:
        # print(len(inserted), flush=True)
        extended = False
        for c in "ACGT":
            nkmer = ckmer + c
            if inserted[nkmer[-k:]] == False:
                inserted[nkmer[-k:]] = True
                inner_seq = inner_seq+c
                ckmer = nkmer[-k:]
                extended = True
                break

        if not extended:
            all_found = False
            if seq == "":
                seq = inner_seq
            else:
                seq = lseq + inner_seq + rseq
            assert len(seq) > k
            for i in range(len(seq)):
                new_found = False
                node = seq[i:i+k-1]
                for c in "ACGT":
                    if not inserted[seq[i:i+k-1]+c]:
                        new_found = True
                        lseq = seq[:i]
                        rseq = seq[i+k-1:]
                        ckmer = seq[i:i+k-1]
                        if len(ckmer) < k-1:
                            all_found = True
                            break
                        inner_seq = seq[i:i+k-1]
                        break
                if new_found:
                    break

            else:
                all_found = True


            if all_found: break


    seq = dna_to_2bits(seq.encode())

    @njit
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, values):
        nkmers = leftsize+m+rightsize-k+1
        update(ht, bt, lock, flag, minimizer, leftpart, leftsize, rightpart, rightsize, values[:nkmers])
        return False


    kmers = compile_superkmer_processor(mask, _func)
    count = 0
    counts = np.zeros(4**k, dtype=np.uint64)
    kmers(seq, 0 , len(seq), ht, bt, lock, np.ones(k-m+1, dtype=np.uint64))
    for st in range(h.subtables):
        for bucket in range(h.nbuckets):
            for slot in range(h.bucketsize):
                if h.private.is_slot_empty_at(ht, st, bucket, slot): break
                for i in range(k-m+1):
                    val = h.private.get_value_at(ht, st, bucket, slot, i)
                    if val == 0: continue
                    if val == 1:
                        print("super")

                    counts[val] += 1
                    count += 1
    for bucket in range(h.backuptable.nbuckets):
        for slot in range(h.backuptable.bucketsize):
            if h.backuptable.private.is_slot_empty_at(bt, bucket, slot): break
            val = h.backuptable.private.get_value_at(bt, bucket, slot)
            if val == 1:
                print("backup")
                sig = h.backuptable.private.get_signature_at(bt, bucket, slot)
                skey = h.backuptable.private.get_subkey_from_bucket_signature(bucket, sig)
                print(skey)

            counts[val] += 1
            count += 1

    print(count)
    print(counts[:5])
    print(4**k)
    assert count == 4**k//2

def test_ecoli_errors():
    """
    superkmer 1:
    GCGG CGTGAACGCCTTATCCGACCT ACG     # stored
    GCGG CGTGAACGCCTTATCCGACCT ACAA    # stored
    GCGG CGTGAACGCCTTATCCGACCT ACGA    # new
    """
    mask = create_mask(25, m=21)
    k = mask.k
    m = mask.m

    # Create hash table
    rcmode = "max"
    n = 2_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    bt = h.backuptable.hashtable
    lock = h.backuplock
    update = h.update
    values = np.ones(k-m+1, dtype=np.uint64)

    update(ht, bt, lock, 0, 1890213221911, 154, 4, 6, 3, values[:4])
    update(ht, bt, lock, 0, 1890213221911, 154, 4, 16, 4, values[:5])
    update(ht, bt, lock, 0, 1890213221911, 154, 4, 24, 4, values[:5])

def test_ecoli_errors2():
    """
    superkmer 1:
    TCTG CGCGGAAATGGACGAACAGTG GGGC    # stored
    TCTG CGCGGAAATGGACGAACAGTG GGGC    # stored
    TCTG CGCGGAAATGGACGAACAGTG GGGA    # stored
    TCTG CGCGGAAATGGACGAACAGTG GGGA    # new
    """
    mask = create_mask(25, m=21)
    k = mask.k
    m = mask.m

    # Create hash table
    rcmode = "max"
    n = 2_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    bt = h.backuptable.hashtable
    lock = h.backuplock
    update = h.update
    values = np.ones(k-m+1, dtype=np.uint64)

    update(ht, bt, lock, 0, 1760997507374, 222, 4, 169, 4, values[:5])
    update(ht, bt, lock, 0, 1760997507374, 222, 4, 169, 4, values[:5])
    update(ht, bt, lock, 0, 1760997507374, 222, 4, 168, 4, values[:5])
    update(ht, bt, lock, 0, 1760997507374, 222, 4, 168, 4, values[:5])

def test_ecoli_errors3():
    """
    TTCC ATCAATTTTTTAAGTTCCATT TG    # inserted/stored V1=[1,1,1]
    TTCC ATCAATTTTTTAAGTTCCATT TG    # inserted/updated V1=[2,2,2]
    TTCC ATCAATTTTTTAAGTTCCATT TTGT  # inserted/updated/stored (CC ATCAATTTTTTAAGTTCCATT TTGT) V1=[3,3,2] V2 = [1,1,1]
    TCCC ATCAATTTTTTAAGTTCCATT TTGT  # isnerted/updated/extended (TTCC ATCAATTTTTTAAGTTCCATT TTGT) V1=[3,3,2] V2 = [1,1,2,2,2]
    TTCC ATCAATTTTTTAAGTTCCATT TTGT  # new V1=[4,4,2] V2 = [1,1,2,2,2]
    """
    mask = create_mask(25, m=21)
    k = mask.k
    m = mask.m

    # Create hash table
    rcmode = "max"
    n = 2_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    bt = h.backuptable.hashtable
    lock = h.backuplock
    update = h.update
    values = np.ones(k-m+1, dtype=np.uint64)

    update(ht, bt, lock, 0, 897647164751, 245, 4, 14, 2, values[:3])
    update(ht, bt, lock, 0, 897647164751, 245, 4, 14, 2, values[:3])
    update(ht, bt, lock, 0, 897647164751, 245, 4, 251, 4, values[:5])
    update(ht, bt, lock, 0, 897647164751, 213, 4, 251, 4, values[:5])
    update(ht, bt, lock, 0, 897647164751, 245, 4, 251, 4, values[:5])

def test_ecoli_errors4():
    return
    """
    TGCG ATCAATTTTTTAAGTTCCATT GTCG # insert
    GTCG ATCAATTTTTTAAGTTCCATT ATTG # insert
    GTCG ATCAATTTTTTAAGTTCCATT GTTG
    GTCG ATCAATTTTTTAAGTTCCATT      # update 2
     TCG ATCAATTTTTTAAGTTCCATT G    # insert new
      CG ATCAATTTTTTAAGTTCCATT GT   # update 1
       G ATCAATTTTTTAAGTTCCATT GTT  # insert new both!
         ATCAATTTTTTAAGTTCCATT GTTG # insert new

    """
    mask = create_mask(25, m=21)
    k = mask.k
    m = mask.m

    # Create hash table
    rcmode = "max"
    n = 2_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    bt = h.backuptable.hashtable
    lock = h.backuplock
    update = h.update
    values = np.ones(k-m+1, dtype=np.uint64)

    update(ht, bt, lock, 0, 1888932688135, 230, 4, 182, 4, values)
    update(ht, bt, lock, 0, 1888932688135, 182, 4, 62, 4, values)
    update(ht, bt, lock, 0, 1888932688135, 182, 4, 190, 4, values)

# def test_strange():
#     """
#         AAG CCCGTGAATATTCACGGGCTT T # kmer
#        AAAG CCCGTGAATATTCACGGGCTT TTTT [1,1,1,1,1] #
#          AG CCCGTGAATATTCACGGGCTT TATG [1,1,1]
#     """
#     mask = create_mask(25, m=21)
#     k = mask.k
#     m = mask.m

#     # Create hash table
#     rcmode = "max"
#     n = 2_000
#     subtables = 1
#     bucketsize = 16
#     hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
#     nvalues = 2**16

#     @njit
#     def update_value(old, new):
#         return uint64(min(old + new, nvalues-1))

#     h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
#     ht = h.hashtable
#     bt = h.backuptable.hashtable
#     lock = h.backuplock
#     update = h.update
#     values = np.ones(k-m+1, dtype=np.uint64)
#     seq =    dna_to_2bits( b"AAGCCCGTGAATATTCACGGGCTTT")
#     seq_rc = dna_to_2bits(b"AAAGCCCGTGAATATTCACGGGCTT")
#     test_seq = dna_to_2bits(b"AAAAGCCCGTGAATATTCACGGGCTTTT")
#     # rev_test_seq = test_seq[::-1]
#     test_seq2 = dna_to_2bits(b"TAAAAGCCCGTGAATATTCACGGGCTTTT")

#     # seq =    dna_to_2bits(b"AAG GTGAATATTCACGGG CTTT")
#     # seq_rc = dna_to_2bits(b"AAAG GTGAATATTCACGGGCT T")
#     @njit
#     def _func(ht1, bt1, lock, flag, minimizer, leftpart, leftsize, rightpart, rightsize, values):
#         print(minimizer)
#         print(leftpart)
#         print(leftsize)
#         print(rightpart)
#         print(rightsize)
#     kmers = compile_superkmer_processor(mask, _func, rcmode=rcmode)
#     print("test seq")
#     kmers(0,0,0,test_seq, 0, len(test_seq),1)
#     print()
#     # kmers(0,0,0,seq_rc, 0, len(seq_rc),1)
#     print("test seq 2")
#     kmers(0,0,0,test_seq2, 0, len(test_seq2),1)
#     # kmers(0,0,0,rev_test_seq, 0, len(rev_test_seq),1)
#     halt()
#     update(ht, bt, lock, 1492555471519, 2, 4, 206, 4, values)
#     update(ht, bt, lock, 1492555471519, 2, 2, 255, 4, values[:3])

#     sig, leftsize, leftpart, rightsize, rightpart, values = h.private.get_item_at(ht, 0, 105, 0)
#     assert leftpart == 2
#     assert leftsize == 4
#     assert rightpart == 206
#     assert rightsize == 4
#     assert (values == [1,1,1,1,1]).all()

#     sig, leftsize, leftpart, rightsize, rightpart, values = h.private.get_item_at(ht, 0, 105, 1)

#     print(leftpart)
#     print(leftsize)
#     print(rightpart)
#     print(rightsize)
#     assert leftpart == 2
#     assert leftsize == 2
#     assert rightpart == 255
#     assert rightsize == 4
#     assert (values == [1,1,1]).all()

#     assert False

def test_flag():
    mask = create_mask(25, m=21)
    k = mask.k
    m = mask.m

    # Create hash table
    rcmode = "max"
    n = 2_000
    subtables = 1
    bucketsize = 16
    hashfuncs = ("linear212315:linear97865421:linear9192741:linear14173245")
    nvalues = 2**16
    stored_values = np.zeros(k-m+1, dtype=np.uint64)

    @njit
    def update_value(old, new):
        return uint64(min(old + new, nvalues-1))

    h = build_hash(k, m, rcmode, n, subtables, bucketsize, hashfuncs, nvalues, update_value)
    ht = h.hashtable
    bt = h.backuptable.hashtable
    lock = h.backuplock
    update = h.update
    values = np.ones(k-m+1, dtype=np.uint64)
    seq = dna_to_2bits(b"AAAAGCCCGTGAATATTCACGGGCTTTT")

    @njit
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, values):
        update(ht, bt, lock, flag, minimizer, leftpart, leftsize, rightpart, rightsize, values[:leftsize+rightsize+m-k+1])
        return False

    kmers = compile_superkmer_processor(mask, _func, rcmode=rcmode)
    kmers(seq, 0, len(seq), ht, bt, lock, values)

    new_minimizer, new_leftsize, new_leftpart, new_rightsize, new_rightpart = h.private.get_item_at(ht, 0, 105, 0, stored_values)
    assert new_leftsize == 2
    assert new_leftpart == 2
    assert new_rightsize == 2
    assert new_rightpart == 15
    assert (stored_values == [2,0,0,0,0]).all()

    assert h.private.is_slot_empty_at(ht, 0, 105,1)
    assert h.private.get_occbits_at(ht, 0, 105,0) == 1

    assert h.backuptable.get_value(bt, 41154593974911) == 2

    assert h.private.get_backupflag_at(ht, 0, 105,0) == 1