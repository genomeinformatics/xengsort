from fastcash.filter_bi_b3c import build_filter
from math import ceil
from numba import njit
from fastcash.lowlevel.debug import set_debugfunctions

sizes = [12, 6, 2]  # filter size in GB
size = sum(sizes)
k = 25
universe = 4**k
nsubfilter = 1

set_debugfunctions(debug=2, timestamps=2)

def test_creater_filter():
    bf = build_filter(k, universe, sizes, nsubfilter, "random")
    t_mem_bytes = ceil(((size * 1024 * 1024 * 1024) // nsubfilter) / 512) * 512 * nsubfilter
    assert bf.mem_bytes == t_mem_bytes

def test_creater_filter_fail():
    sizes = [12, 6, 2, 1]
    size = sum(sizes)
    try:
        bf = build_filter(k, universe, sizes, nsubfilter, "random")
    except ValueError:
        pass
    else:
        assert False
def test_set_get_value_in_subfilter():
    bf = build_filter(k, universe, sizes, 9, "random")

    key = 16034544569
    bf.private.set_value_in_subfilter(bf.filter_array, 1, key)
    print( bf.private.get_value_in_subfilter(bf.filter_array, 1, key))
    assert bf.private.get_value_in_subfilter(bf.filter_array, 1, key)

test_set_get_value_in_subfilter()

def test_set_get():
    assert False
    bf = build_filter(k, universe, sizes, nsubfilter, "random")

    key = 16034544569
    bf.store_new(bf.filter_array, key)
    bf.store_new(bf.filter_array, key)
    bf.store_new(bf.filter_array, key)
    assert bf.get_value(bf.filter_array, key)

    get_value_at = bf.private.get_value_at
    b = bf.filterbits

    @njit()
    def count(a):
        c = 0
        for i in range(b):
            c += get_value_at(a, i)
        return c

    count_ones = count(bf.filter_array)
    print(count_ones)
    assert count_ones == 3