from argparse import ArgumentParser
from fastcash.hashio import load_hash
from fastcash.dnaencode import dna_to_2bits

def get_argument_parser():
    p = ArgumentParser()
    p.add_argument("--index", "-i", metavar="INDEX_HDF5", default="index.h5",
        help="name of the index HDF5 file (input)")
    return p


def main():
    p = get_argument_parser()
    args = p.parse_args()
    # load hash tabel
    h, valueinfo, info = load_hash(args.index)
    ht = h.hashtable
    k = int(info['k'])
    mask = 2**(2*k)-1

    fqseq = b"GAATCNCCAGGAAGGTGAACAAACCTTTGCTGAACACCTTCATAAAATTCAGCGCGTCGGGTAGCGTGCCCTGAGCATCTGCCGGAACGTGCATCACCGTGGCAATTAACGTCGCTATCCCCAGCTGCAGACCGGCGGCAATAAAACCGGG"
    faseq = b"AACGGTGCGGGCTGACGCGTACAGGAAACACAGAAAAAAGCCCGCACCTGACAGTGCGGGCTTTTTTTTTCGACCAAAGG"
    seq = faseq
    seq = dna_to_2bits(seq)
    
    for i in range(len(seq)-k+1):
        kmer = seq[i:k+i]
        print(kmer)

        # codes = dna_to_2bits(kmer)
        codes = kmer
        kcode = 0
        for i in codes:
            kcode = kcode << 2
            kcode += i
        print(h.get_value_and_choice(ht, kcode))


if __name__ == "__main__":
    main()