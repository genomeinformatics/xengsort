import numpy as np
from numba import njit, uint64
from math import ceil, log2

from construx.hashfunctions import parse_names, build_get_bucket_fpr
from construx.srhash import get_nbuckets
from construx.dnaio import fasta_reads
from construx.build import compile_kmer_iterator
from construx.dnaencode import dna_to_2bits
from construx.mathutils import bitsfor
def test_stable_dist():
    k = 31
    nobjects = 3102953939 # 4641622
    bucketsize = 8
    fill = 0.9
    subtables = 8
    #get hashfunctions
    hashfuncs = parse_names("default", 3)
    universe = int(4**k)
    nbuckets = get_nbuckets(nobjects, bucketsize, fill) * bucketsize
    nbuckets = uint64(ceil(nbuckets/8))
    print("#nbuckets:", nbuckets)
    get_bf1, get_key1 = build_get_bucket_fpr(hashfuncs[0], universe, nbuckets, subtables=subtables)
    get_bf2, get_key2 = build_get_bucket_fpr(hashfuncs[1], universe, nbuckets, subtables=subtables)
    get_bf3, get_key3 = build_get_bucket_fpr(hashfuncs[2], universe, nbuckets, subtables=subtables)

    table = np.zeros(subtables, dtype=np.uint64)
    k, kmers = compile_kmer_iterator(k, rcmode="max")

    @njit
    def count_table(table, seq):
        for kmer in kmers(seq, 0, len(seq)):
            s1, p1, f1 = get_bf1(kmer)
            s2, p2, f2 = get_bf2(kmer)
            s3, p3, f3 = get_bf3(kmer)
            assert s1==s2==s3
            assert s1 >= 0
            assert s1 < subtables
            assert get_key1(s1, p1, f1) == kmer
            assert get_key2(s2, p2, f2) == kmer
            assert get_key3(s3, p3, f3) == kmer
            table[s1] += 1

    for _, seq in fasta_reads("Homo_sapiens.GRCh38.dna.toplevel.fa.gz", sequences_only=False):
    #for _, seq in fasta_reads("ecoli.fa.gz", sequences_only=False):
        seq = dna_to_2bits(seq)
        count_table(table, seq)
    print(table)

def test_linear(subtables):
    k = 31
    nobjects = 3102953939 # 4641622
    bucketsize = 8
    fill = 0.9

    universe = int(4**k)
    nbuckets = get_nbuckets(nobjects, bucketsize, fill) * bucketsize
    nbuckets = uint64(ceil(nbuckets/subtables))
    print("#nbuckets:", nbuckets)

    table = np.zeros(subtables, dtype=np.uint64)
    k, kmers = compile_kmer_iterator(k, rcmode="max")
    subtablemask = subtables-1
    subtablebits = int(log2(subtables))
    qbits = bitsfor(universe)
    codemask = uint64(2**qbits - 1)

    a = 984365
    b = 3917514

    @njit(locals=dict(swap=uint64))
    def count_table(table, seq):
        for kmer in kmers(seq, 0, len(seq)):
            swap = ((kmer << k) ^ (kmer >> k))
            swap = (a*swap) & codemask
            s = swap  >> (2*k - subtablebits)
            assert s >= 0
            assert s < 8
            table[s] += 1

    for _, seq in fasta_reads("Homo_sapiens.GRCh38.dna.toplevel.fa.gz", sequences_only=False):
    #for _, seq in fasta_reads("ecoli.fa.gz", sequences_only=False):
        seq = dna_to_2bits(seq)
        count_table(table, seq)
    print(table)

def test_swap(subtables):
    k = 31
    nobjects = 3102953939 # 4641622
    bucketsize = 8
    fill = 0.9

    universe = int(4**k)
    nbuckets = get_nbuckets(nobjects, bucketsize, fill) * bucketsize
    nbuckets = uint64(ceil(nbuckets/subtables))
    print("#nbuckets:", nbuckets)

    table = np.zeros(subtables, dtype=np.uint64)
    k, kmers = compile_kmer_iterator(k, rcmode="max")
    subtablemask = subtables-1
    subtablebits = int(log2(subtables))
    qbits = bitsfor(universe)
    codemask = uint64(2**qbits - 1)

    a = 984365
    b = 3917514

    @njit(locals=dict(swap=uint64))
    def count_table(table, seq):
        for kmer in kmers(seq, 0, len(seq)):
            swap = ((kmer << k) ^ (kmer >> k))
            swap = a*(swap ^ b) & codemask
            s = swap  >> (2*k - subtablebits)
            assert s >= 0
            assert s < 8
            table[s] += 1

    for _, seq in fasta_reads("Homo_sapiens.GRCh38.dna.toplevel.fa.gz", sequences_only=False):
    #for _, seq in fasta_reads("ecoli.fa.gz", sequences_only=False):
        seq = dna_to_2bits(seq)
        count_table(table, seq)
    print(table)

def test_swap_mod(subtables):
    k = 31
    nobjects = 3102953939 # 4641622
    bucketsize = 8
    fill = 0.9

    universe = int(4**k)
    nbuckets = get_nbuckets(nobjects, bucketsize, fill) * bucketsize
    nbuckets = uint64(ceil(nbuckets/subtables))
    print("#nbuckets:", nbuckets)

    table = np.zeros(subtables, dtype=np.uint64)
    k, kmers = compile_kmer_iterator(k, rcmode="max")
    subtablemask = subtables-1

    a = 984365
    b = 3917514

    @njit
    def count_table(table, seq):
        for kmer in kmers(seq, 0, len(seq)):
            swap = ((kmer << k) ^ (kmer >> k))
            s = a*(swap ^ b) % subtables # & subtablemask
            table[s] += 1

    for _, seq in fasta_reads("Homo_sapiens.GRCh38.dna.toplevel.fa.gz", sequences_only=False):
    #for _, seq in fasta_reads("ecoli.fa.gz", sequences_only=False):
        seq = dna_to_2bits(seq)
        count_table(table, seq)
    print(table)


if __name__ == "__main__":
    #test_stable_dist()
    test_linear(8)

