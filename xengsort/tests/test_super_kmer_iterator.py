from fastcash.superkmers import compile_superkmer_processor
from fastcash.dnaencode import dna_to_2bits, compile_revcomp_and_canonical_code, revcomp_code, qcode_to_dnastr
from fastcash.mask import create_mask

from numba import njit, uint8, uint64
import numpy as np

def test_one_kmer():
    # provide one k-mer with and the minimizer is in the middle
    @njit(locals=dict(leftsize=uint8, rightsize=uint8, minimizer=uint64, rightpart=uint64, leftpart=uint64))
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, values):
        values[0] = leftpart
        values[1] = minimizer
        values[2] = rightpart
        values[3] = leftsize
        values[4] = rightsize
        values[5] += 1
        values[6] = flag
        # print(f"{leftpart=}")
        # print(f"{minimizer=}")
        # print(f"{rightpart=}")
        # print(f"{leftsize=}")
        # print(f"{rightsize=}")
        return False

    mask = create_mask(25, m=17)
    kmers = compile_superkmer_processor(mask, _func, rcmode="max")

    ht = None
    bt = None
    lock = None
    values = np.zeros(7,dtype=np.uint64)
    seq = dna_to_2bits(b"TTTTCTTTTTTTTTTTTTTTCTTCT")
    kmers(seq, 0, 25, ht, bt, lock, values)
    assert values[0] == uint64(32) # int("00100000",2)
    assert values[1] == 8589934594
    assert values[2] == 0 # int("00000000",2)
    assert values[3] == 4
    assert values[4] == 4
    assert values[5] == 1
    assert values[6] == 0

def test_max_super_kmer():
    # one super k-mer with max size
    @njit(locals=dict(leftsize=uint8, rightsize=uint8, minimizer=uint64, rightpart=uint64, leftpart=uint64))
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, values):
        values[0] = leftpart
        values[1] = minimizer
        values[2] = rightpart
        values[3] = leftsize
        values[4] = rightsize
        values[5] += 1
        values[6] = flag
        return False

    mask = create_mask(25, m=17)
    kmers = compile_superkmer_processor(mask, _func, rcmode="max")
    ht = None
    bt = None
    lock = None
    values = np.zeros(7,dtype=np.uint64)
    seq = dna_to_2bits(b"TTTTTTTTCTTTTTTTTTTTTTTTCTTTTTTTT")
    kmers(seq, 0, len(seq), ht, bt, lock, values)
    assert values[0] == 0
    assert values[1] == 8589934594
    assert values[2] == 0
    assert values[3] == 8
    assert values[4] == 8
    assert values[5] == 1
    assert values[6] == 0


def test_get_most_left_minimizer():
    mask = create_mask(25, m=17)
    k = mask.k
    m = mask.m

    seq = dna_to_2bits(b"TGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGT")
    size = len(seq)


    @njit(locals=dict(leftsize=uint8, rightsize=uint8, minimizer=uint64, rightpart=uint64, leftpart=uint64))
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, lps, mins, rps, lss, rss, flags, count):
        lps[count] = leftpart
        mins[count] = minimizer
        rps[count] = rightpart
        lss[count] = leftsize
        rss[count] = rightsize
        flags[count] = flag
        count[0] += 1
        return False

    @njit(locals=dict(value=uint64, size=uint8))
    def _decode(value, size):
        a = np.zeros(size, dtype=np.uint8)
        for i in range(size):
            a[size - i -1] = (value >> (2*i)) & 3
        return a

    leftparts = np.zeros(size-k+1,dtype=np.uint64)
    minimizers = np.zeros(size-k+1,dtype=np.uint64)
    rightparts = np.zeros(size-k+1,dtype=np.uint64)
    leftsizes = np.zeros(size-k+1,dtype=np.uint8)
    rightsizes = np.zeros(size-k+1,dtype=np.uint8)
    flags = np.zeros(size-k+1,dtype=np.uint8)
    count = np.zeros(1,dtype=np.uint64)
    kmers = compile_superkmer_processor(mask, _func, rcmode="max")
    ht = 0
    bt = None
    lock = None

    kmers(seq, 0, len(seq), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    count = int(count[0])
    assert count == size-k+1
    assert (leftparts[count:]== 0).all()
    assert (minimizers[count:]== 0).all()
    assert (rightparts[count:]== 0).all()
    assert (leftsizes[count:]== 0).all()
    assert (rightsizes[count:]== 0).all()

    print(count)
    assert (flags == 1).all()

    assert (leftparts[0]== int("3",4))
    assert (minimizers[0]== int("22222222222222222",4))
    assert (rightparts[0]== int("2222222",4))
    assert (leftsizes[0]== 1)
    assert (rightsizes[0]== 7)

    for i in range(1,count-1):
        assert (leftparts[i]== 0)
        assert (minimizers[i]== int("22222222222222222",4))
        assert (rightparts[i]== int("22222222",4))
        assert (leftsizes[i]== 0)
        assert (rightsizes[i]== 8)

    assert (leftparts[count-1]== 0)
    assert (minimizers[count-1]== int("22222222222222222",4))
    assert (rightparts[count-1]== int("22222223",4))
    assert (leftsizes[count-1]== 0)
    assert (rightsizes[count-1]== 8)

def test_overlapp():
    mask = create_mask(25, m=17)
    k = mask.k
    m = mask.m
    # mask = create_mask(7)
    # k = mask.k
    # m = 3
    size = 50_000
    gc = 0.4
    at = 1-gc
    # seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)
    np.random.seed(42)
    seq = np.random.choice([0,1,2,3],p=[at/2, gc/2, gc/2, at/2],size=size)



    @njit(locals=dict(leftsize=uint8, rightsize=uint8, minimizer=uint64, rightpart=uint64, leftpart=uint64))
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, lps, mins, rps, lss, rss, flags, count):
        lps[count] = leftpart
        mins[count] = minimizer
        rps[count] = rightpart
        lss[count] = leftsize
        rss[count] = rightsize
        flags[count] = flag
        count[0] += 1
        return False

    @njit(locals=dict(value=uint64, size=uint8))
    def _decode(value, size):
        a = np.zeros(size, dtype=np.uint8)
        for i in range(size):
            a[size - i -1] = (value >> (2*i)) & 3
        return a

    leftparts = np.zeros(size-k+1,dtype=np.uint64)
    minimizers = np.zeros(size-k+1,dtype=np.uint64)
    rightparts = np.zeros(size-k+1,dtype=np.uint64)
    leftsizes = np.zeros(size-k+1,dtype=np.uint8)
    rightsizes = np.zeros(size-k+1,dtype=np.uint8)
    flags = np.zeros(size-k+1,dtype=np.uint8)
    count = np.zeros(1,dtype=np.uint64)
    kmers = compile_superkmer_processor(mask, _func, rcmode="max")
    ht = 0
    bt = None
    lock = None

    kmers(seq, 0, len(seq), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    count = np.uint64(count[0])
    assert (leftparts[count:]== 0).all()
    assert (minimizers[count:]== 0).all()
    assert (rightparts[count:]== 0).all()
    assert (leftsizes[count:]== 0).all()
    assert (rightsizes[count:]== 0).all()

    print(f"{count=}")
    print(f"{type(count)=}")

    # check overlap of super-k-mers
    for i in range(count-np.uint64(1)):
        skmer1 = np.zeros(leftsizes[i]+m+rightsizes[i], dtype=np.uint8)
        skmer1[:leftsizes[i]] = _decode(leftparts[i], leftsizes[i])
        skmer1[leftsizes[i]:leftsizes[i]+m] = _decode(minimizers[i], m)
        skmer1[leftsizes[i]+m:] = _decode(rightparts[i], rightsizes[i])

        rcskmer1 = np.zeros(leftsizes[i]+m+rightsizes[i], dtype=np.uint8)
        rcskmer1[:rightsizes[i]] = _decode(revcomp_code(rightparts[i], rightsizes[i]), rightsizes[i])
        rcskmer1[rightsizes[i]:rightsizes[i]+m] = _decode(revcomp_code(minimizers[i], m), m)
        rcskmer1[rightsizes[i]+m:] = _decode(revcomp_code(leftparts[i], leftsizes[i]), leftsizes[i])

        skmer2 = np.zeros(leftsizes[i+1]+m+rightsizes[i+1], dtype=np.uint8)
        skmer2[:leftsizes[i+1]] = _decode(leftparts[i+1], leftsizes[i+1])
        skmer2[leftsizes[i+1]:leftsizes[i+1]+m] = _decode(minimizers[i+1], m)
        skmer2[leftsizes[i+1]+m:] = _decode(rightparts[i+1], rightsizes[i+1])

        print("leftsizes: ", leftsizes[i+1])
        print("leftsizes: ", leftsizes[i+1])
        print("rightsize: ", rightsizes[i+1])
        print("rightpart: ", rightparts[i+1])
        # print("rightpart: ", bin(leftparts[i+1])[2:])
        rcskmer2 = np.zeros(leftsizes[i+1]+m+rightsizes[i+1], dtype=np.uint8)
        print("geht")
        print(revcomp_code(rightparts[i+1], rightsizes[i+1]))
        rcskmer2[:rightsizes[i+1]] = _decode(revcomp_code(rightparts[i+1], rightsizes[i+1]), rightsizes[i+1])
        rcskmer2[rightsizes[i+1]:rightsizes[i+1]+m] = _decode(revcomp_code(minimizers[i+1], m), m)
        rcskmer2[rightsizes[i+1]+m:] = _decode(revcomp_code(leftparts[i+1], leftsizes[i+1]), leftsizes[i+1])

        print(skmer1)
        print(rcskmer1)
        print(skmer2)
        print(rcskmer2)

        dist_between_minimizers = rightsizes[i]
        overlap =leftsizes[i+1]+m-1
        left_overlap = skmer1[-(k-1):]
        assert len(left_overlap) == k-1
        right_overlap = skmer2[:k-1]
        assert len(right_overlap) == k-1
        s1f_s2f = (skmer1[-(k-1):] == skmer2[:k-1]).all()
        s1f_s2rc = (skmer1[-(k-1):] == rcskmer2[:k-1]).all()
        s1rc_s2f = (rcskmer1[-(k-1):] == skmer2[:k-1]).all()
        s1rc_s2rc = (rcskmer1[-(k-1):] == rcskmer2[:k-1]).all()
        assert s1f_s2f or s1f_s2rc or s1rc_s2f or s1rc_s2rc

def test_N():
    @njit(locals=dict(leftsize=uint8, rightsize=uint8, minimizer=uint64, rightpart=uint64, leftpart=uint64))
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, lps, mins, rps, lss, rss, flags, count):
        lps[count] = leftpart
        mins[count] = minimizer
        rps[count] = rightpart
        lss[count] = leftsize
        rss[count] = rightsize
        flags[count] = flag
        count[0] += 1
        return False

    seq = dna_to_2bits(b"NNNNNNNNHNNNNNGNNNNNNNTNNNNNNNNNNNNNNNNNNNNNANNNNNNNNNNNNNNTTTTTTTTCTTTTTTTTTTTTTTTCTTTTTTTTNTTTTTTTTCTTTTTTTTTTTTTTTCTTTTTTTTNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
    mask = create_mask(25, m=17)
    k = mask.k
    m = mask.m
    size = len(seq)
    leftparts = np.zeros(size-k+1,dtype=np.uint64)
    minimizers = np.zeros(size-k+1,dtype=np.uint64)
    rightparts = np.zeros(size-k+1,dtype=np.uint64)
    leftsizes = np.zeros(size-k+1,dtype=np.uint8)
    rightsizes = np.zeros(size-k+1,dtype=np.uint8)
    flags = np.zeros(size-k+1,dtype=np.uint8)
    count = np.zeros(1,dtype=np.uint64)
    kmers = compile_superkmer_processor(mask, _func, rcmode="max")
    ht = 0
    bt = None
    lock = None

    kmers(seq, 0, len(seq), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    count = np.uint64(count[0])
    assert (leftparts[count:]== 0).all()
    assert (minimizers[count:]== 0).all()
    assert (rightparts[count:]== 0).all()
    assert (leftsizes[count:]== 0).all()
    assert (rightsizes[count:]== 0).all()

    assert (flags == 0).all()

    assert (leftparts[0]== 0)
    assert (minimizers[0]== 8589934594)
    assert (rightparts[0]== 0)
    assert (leftsizes[0]== 8)
    assert (rightsizes[0]== 8)


    assert (leftparts[1]== 0)
    assert (minimizers[1]== 8589934594)
    assert (rightparts[1]== 0)
    assert (leftsizes[1]== 8)
    assert (rightsizes[1]== 8)

def test_minimizer_twice_fwd_revcomp_1():
    @njit(locals=dict(leftsize=uint8, rightsize=uint8, minimizer=uint64, rightpart=uint64, leftpart=uint64))
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, lps, mins, rps, lss, rss, flags, count):
        print(minimizer)
        print(leftpart)
        print(leftsize)
        print(rightpart)
        print(rightsize)
        print(flag)
        lps[count] = leftpart
        mins[count] = minimizer
        rps[count] = rightpart
        lss[count] = leftsize
        rss[count] = rightsize
        flags[count] = flag
        count[0] += 1
        return False

    seq1 = dna_to_2bits(b"AAGCCCGTGAATATTCACGGGCTTT") # 1 k-mer and flag
    seq2 = dna_to_2bits(b"AAAGCCCGTGAATATTCACGGGCTT") # 1 k-mer and flag
    seq3 = dna_to_2bits(b"AAAAGCCCGTGAATATTCACGGGCT") # 1 k-mer and no flag
    seq4 = dna_to_2bits(b"AAAAGCCCGTGAATATTCACGGGCTT") # 2 k-mer no flag, flag
    seq5 = dna_to_2bits(b"AAAAGCCCGTGAATATTCACGGGCTTTT") # 1 k-mer and flag, 1k-mer and flag, 1k-mer and no flag
    seq6 = dna_to_2bits(b"AAGCCGGATGATCATCCGGCTTTTT")
    seq7 = dna_to_2bits(b"GCTTACCCTGAATATTCAGGGTAAG")
    # 1 kmer and no flag,
    # minimizer AAGCGGATGATCATCCGGCTT
    # rev minimizer AGCGGATGATCATCCGGCTTT
    mask = create_mask(25, m=21)
    k = mask.k
    m = mask.m
    size = len(seq1)+len(seq2)+len(seq3)+len(seq4)+len(seq5)+len(seq6)
    leftparts = np.zeros(size-k+1,dtype=np.uint64)
    minimizers = np.zeros(size-k+1,dtype=np.uint64)
    rightparts = np.zeros(size-k+1,dtype=np.uint64)
    leftsizes = np.zeros(size-k+1,dtype=np.uint8)
    rightsizes = np.zeros(size-k+1,dtype=np.uint8)
    flags = np.zeros(size-k+1,dtype=np.uint8)
    count = np.zeros(1,dtype=np.uint64)
    kmers = compile_superkmer_processor(mask, _func, rcmode="max")
    ht = 0
    bt = None
    lock = None

    kmers(seq1, 0, len(seq1), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    assert int(count[0]) == 1
    assert flags[0] == 1

    kmers(seq2, 0, len(seq2), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    assert int(count[0]) == 2
    assert flags[1] == 1

    kmers(seq3, 0, len(seq3), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    assert int(count[0]) == 3
    assert flags[2] == 0

    kmers(seq4, 0, len(seq4), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    assert int(count[0]) == 5
    assert flags[3] == 0
    assert flags[4] == 1

    kmers(seq5, 0, len(seq5), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    assert int(count[0]) == 9
    assert flags[5] == 0
    assert flags[6] == 1
    assert flags[7] == 1
    assert flags[8] == 0

    kmers(seq6, 0, len(seq6), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    assert int(count[0]) == 10
    assert flags[9] == 1

    kmers(seq7, 0, len(seq7), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    assert int(count[0]) == 11
    assert flags[9] == 1

def test_fwd_revcomp_in_seq():
    @njit(locals=dict(leftsize=uint8, rightsize=uint8, minimizer=uint64, rightpart=uint64, leftpart=uint64))
    def _func(minimizer, leftpart, leftsize, rightpart, rightsize, flag, ht, bt, lock, lps, mins, rps, lss, rss, flags, count):
        lps[count] = leftpart
        mins[count] = minimizer
        rps[count] = rightpart
        lss[count] = leftsize
        rss[count] = rightsize
        flags[count] = flag
        count[0] += 1
        return False

    seq = dna_to_2bits(b"TCTCTCT")
    seq = dna_to_2bits(b"AGAGAGA")
    mask = create_mask(7, m=3)
    k = mask.k
    m = mask.m
    size = len(seq)
    leftparts = np.zeros(size-k+1,dtype=np.uint64)
    minimizers = np.zeros(size-k+1,dtype=np.uint64)
    rightparts = np.zeros(size-k+1,dtype=np.uint64)
    leftsizes = np.zeros(size-k+1,dtype=np.uint8)
    rightsizes = np.zeros(size-k+1,dtype=np.uint8)
    flags = np.zeros(size-k+1,dtype=np.uint8)
    count = np.zeros(1,dtype=np.uint64)
    kmers = compile_superkmer_processor(mask, _func, rcmode="max")
    ht = 0
    bt = None
    lock = None

    kmers(seq, 0, len(seq), ht, bt, lock, leftparts, minimizers, rightparts, leftsizes, rightsizes, flags, count)
    count = np.uint64(count[0])
    assert (leftparts[count:]== 0).all()
    assert (minimizers[count:]== 0).all()
    assert (rightparts[count:]== 0).all()
    assert (leftsizes[count:]== 0).all()
    assert (rightsizes[count:]== 0).all()

    assert (flags[0:1] == 1).all()
    assert minimizers[0] == 34