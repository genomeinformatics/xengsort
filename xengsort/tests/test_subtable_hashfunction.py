from fastcash.subtable_hashfunctions import build_get_sub_bucket_fpr_from_key, build_get_sub_subkey_from_key, build_get_bucket_fpr_from_subkey, build_get_sub_buckets_fprs_from_key
from math import log

def test_build_get_sub_bucket_fpr_from_key():
    k=28
    universe = int(4**k)
    nbuckets = 41231
    subtables = 5
    code = 42591956679675543
    # linear
    name = "linear371293"
    tablename = "linear1847183"
    get_sub_bucket_fpr, get_key = build_get_sub_bucket_fpr_from_key(tablename, name, universe, nbuckets, subtables)
    s, p, f = get_sub_bucket_fpr(code)
    key = get_key(s, p, f)
    assert key == code

    # affine
    name = "affine371293-418237"
    get_sub_bucket_fpr, get_key = build_get_sub_bucket_fpr_from_key(tablename, name, universe, nbuckets, subtables)
    s, p, f = get_sub_bucket_fpr(code)
    key = get_key(s, p, f)
    assert key == code

def test_build_get_sub_subkey_from_key():
    k=28
    universe = int(4**k)
    nbuckets = 41231
    subtables = 5
    code = 42591956679675543
    # linear
    name = "linear371293"
    tablename = "linear1847183"
    get_sub_subkey, get_key = build_get_sub_subkey_from_key(name, universe, nbuckets, subtables)
    s, subkey = get_sub_subkey(code)
    key = get_key(s, subkey)
    assert key == code

    # affine
    name = "affine371293-418237"
    get_sub_subkey, get_key = build_get_sub_subkey_from_key(name, universe, nbuckets, subtables)
    s, subkey = get_sub_subkey(code)
    key = get_key(s, subkey)
    assert key == code

def test_build_get_bucket_fpr_from_subkey():
    k=28
    universe = int(4**k)
    nbuckets = 41231
    subtables = 5
    code = 42591956679675543
    subkey = code // subtables
    # linear
    name = "linear371293"
    get_bucket_fpr, get_subkey = build_get_bucket_fpr_from_subkey(name, universe, nbuckets, subtables)
    bucket, fpr = get_bucket_fpr(subkey)
    newsubkey = get_subkey(bucket, fpr)
    assert newsubkey == subkey

    # affine
    name = "affine371293-418237"
    get_bucket_fpr, get_subkey = build_get_bucket_fpr_from_subkey(name, universe, nbuckets, subtables)
    bucket, fpr = get_bucket_fpr(subkey)
    newsubkey = get_subkey(bucket, fpr)
    assert newsubkey == subkey

def test_build_get_sub_buckets_fprs_from_key():
    k=28
    universe = int(4**k)
    nbuckets = 41231
    subtables = 5
    code = 42591956679675543
    names = "linear1481923:affine371293-418237:linear371293:affine42309417-132489"
    tablename = "linear1481923"
    name1 = "affine371293-418237"
    name2 = "linear371293"
    name3 = "affine42309417-132489"
    get_sub_buckets_fprs, get_key = build_get_sub_buckets_fprs_from_key(names, universe, nbuckets, subtables)
    get_sub_bucket_fpr1, get_key1 = build_get_sub_bucket_fpr_from_key(tablename, name1, universe, nbuckets, subtables)
    get_sub_bucket_fpr2, get_key2 = build_get_sub_bucket_fpr_from_key(tablename, name2, universe, nbuckets, subtables)
    get_sub_bucket_fpr3, get_key3 = build_get_sub_bucket_fpr_from_key(tablename, name3, universe, nbuckets, subtables)

    sub, bucket1, fpr1, bucket2, fpr2, bucket3, fpr3 = get_sub_buckets_fprs(code)
    s1, p1, f1 = get_sub_bucket_fpr1(code)
    s2, p2, f2 = get_sub_bucket_fpr2(code)
    s3, p3, f3 = get_sub_bucket_fpr3(code)

    assert sub == s1 == s2 == s3
    assert p1 == bucket1
    assert f1 == fpr1
    assert p2 == bucket2
    assert f2 == fpr2
    assert p3 == bucket3
    assert f3 == fpr3

    assert code == get_key(sub, bucket1, fpr1, 1)
    assert code == get_key(sub, bucket2, fpr2, 2)
    assert code == get_key(sub, bucket3, fpr3, 3)
