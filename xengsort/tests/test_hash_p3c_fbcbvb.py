from fastcash.hash_s3c_fbcbvb import build_hash
# from fastcash.parameters import get_valueset_parameters
from fastcash.parameters import get_valueset_and_parameters
from fastcash.srhash import get_nbuckets

k = 29
universe = 4**k
nobjects = 1378912
fill = 0.9
subtables = 5
bucketsize = 4
hashfuncs = "affine13419-6523123:linear23487:linear8971:linear98721"
n = get_nbuckets(nobjects, bucketsize, fill) * bucketsize

P = get_valueset_and_parameters(["test", "2"], mask=k, rcmode="max")
(values, valuestr, rcmode, k) = P
nvalues = values.NVALUES
update_value = values.update


######### PRIVATE FUNCTIONS #########

def test_set_get_value_at():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    value = 3
    subtable = 1
    bucket = 1
    slot = 1
    get_value = h.private.get_value_at 
    set_value = h.private.set_value_at
    assert get_value(h.hashtable, subtable, bucket, slot) == 0
    set_value(h.hashtable, subtable, bucket, slot, value)
    assert get_value(h.hashtable, subtable, bucket, slot) == value

    value = 5 # invalid vale
    set_value(h.hashtable, subtable, bucket, slot, value)
    assert get_value(h.hashtable, subtable, bucket, slot) != value

def test_choice_fpr_signature():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    subtable = 1
    bucket = 1
    slot = 1
    choice = 2
    fpr = 4123
    signature_from_choice_fpr = h.private.signature_from_choice_fingerprint
    signature_to_choice_fpr = h.private.signature_to_choice_fingerprint
    sig = signature_from_choice_fpr(choice, fpr)
    newchoice, newfpr = signature_to_choice_fpr(sig)
    assert choice == newchoice
    assert fpr == newfpr

def test_set_get_signature_at():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    subtable = 1
    bucket = 1
    slot = 1
    choice = 2
    fpr = 4123
    set_sig_at = h.private.set_signature_at
    get_sig_at = h.private.get_signature_at
    signature_from_choice_fpr = h.private.signature_from_choice_fingerprint
    signature_to_choice_fpr = h.private.signature_to_choice_fingerprint
    assert get_sig_at(h.hashtable, subtable, bucket, slot) == 0
    sig = signature_from_choice_fpr(choice, fpr)
    set_sig_at(h.hashtable, subtable, bucket, slot, sig)
    assert get_sig_at(h.hashtable, subtable, bucket, slot) == sig

    get_choicebits_at = h.private.get_choicebits_at
    assert choice == get_choicebits_at(h.hashtable, subtable, bucket, slot)

def test_set_get_item_at():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    subtable = 1
    bucket = 1
    slot = 1
    choice = 2
    fpr = 4123
    value = 3
    set_item_at = h.private.set_item_at
    get_item_at = h.private.get_item_at
    get_sig_at = h.private.get_signature_at
    get_value_at = h.private.get_value_at
    signature_from_choice_fpr = h.private.signature_from_choice_fingerprint
    newsig, newvalue = get_item_at(h.hashtable, subtable, bucket, slot) 
    assert newsig == 0
    assert newvalue == 0
    sig = signature_from_choice_fpr(choice, fpr)
    set_item_at(h.hashtable, subtable, bucket, slot, sig, value)
    newsig, newvalue = get_item_at(h.hashtable, subtable, bucket, slot) 
    assert newsig == sig
    assert newvalue == value
    assert get_sig_at(h.hashtable, subtable, bucket, slot) == sig
    assert get_value_at(h.hashtable, subtable, bucket, slot) == value


def test_get_subkey_from_signature():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    subtable = 1
    bucket = 1
    slot = 1
    choice = 2
    subkey = 4123
    value = 3
    get_subkey_from_signature = h.private.get_subkey_from_bucket_signature
    get_subkey_choice_from_bucket_signature = h.private.get_subkey_choice_from_bucket_signature

    get_bs = h.private.get_bs
    bucket, sig = get_bs[choice](subkey)
    newsubkey = get_subkey_from_signature(bucket, sig)
    assert subkey == newsubkey
    newsubkey, newchoice = get_subkey_choice_from_bucket_signature(bucket, sig)
    assert newsubkey == subkey
    assert newchoice == choice+1

######### PUBLIC FUNCTIONS #########

def test_update():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    key = 98104713013
    value = 1
    choice = 0

    update = h.private.update_ssk
    get_subtable_subkey = h.private.get_subtable_subkey_from_key
    get_bs = h.private.get_bs[choice]
    set_signature_at = h.private.set_signature_at
    set_value_at = h.private.set_value_at
    get_value_at = h.private.get_value_at

    # existing subkey
    subtable, subkey = get_subtable_subkey(key)
    bucket, sig = get_bs(subkey)
    set_signature_at(h.hashtable, subtable, bucket, 0, sig)
    set_value_at(h.hashtable, subtable, bucket, 0, value)
    status, result = update(h.hashtable, subtable, subkey, 2)
    assert status - 128 > 0
    assert get_value_at(h.hashtable, subtable, bucket, 0) == result

    # subkey is not contained in the table
    subtable, subkey = get_subtable_subkey(key+1)
    bucket, sig = get_bs(subkey)
    status, result = update(h.hashtable, subtable, subkey, 2)
    assert status == 1
    assert get_value_at(h.hashtable, subtable, bucket, 0) == 2


def test_update_existing():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    key = 98104713013
    value = 1
    choice = 0

    update = h.private.update_existing_ssk
    get_subtable_subkey = h.private.get_subtable_subkey_from_key
    get_bs = h.private.get_bs[choice]
    set_signature_at = h.private.set_signature_at
    set_value_at = h.private.set_value_at
    get_value_at = h.private.get_value_at

    # existing subkey
    subtable, subkey = get_subtable_subkey(key)
    bucket, sig = get_bs(subkey)
    set_signature_at(h.hashtable, subtable, bucket, 0, sig)
    set_value_at(h.hashtable, subtable, bucket, 0, value)
    status, result = update(h.hashtable, subtable, subkey, 2)
    assert status - 128 > 0
    assert get_value_at(h.hashtable, subtable, bucket, 0) == result

    # subkey is not contained in the table
    subtable, subkey = get_subtable_subkey(key+1)
    bucket, sig = get_bs(subkey)
    status, result = update(h.hashtable, subtable, subkey, 2)
    assert status == 0
    assert get_value_at(h.hashtable, subtable, bucket, 0) == 0

def test_store_new():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    key = 98104713013
    value = 1
    choice = 0

    store_new = h.private.store_new_ssk
    get_subtable_subkey = h.private.get_subtable_subkey_from_key
    get_bs = h.private.get_bs[choice]
    set_signature_at = h.private.set_signature_at
    set_value_at = h.private.set_value_at
    get_value_at = h.private.get_value_at

    subtable, subkey = get_subtable_subkey(key)
    bucket, sig = get_bs(subkey)
    status, steps = store_new(h.hashtable, subtable, subkey, 2)
    assert steps == 1
    assert status == 1
    assert get_value_at(h.hashtable, subtable, bucket, 0) == 2

def test_overwrite():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    key = 98104713013
    value = 1
    choice = 0

    store_new = h.private.store_new_ssk
    overwrite = h.private.overwrite_ssk
    get_subtable_subkey = h.private.get_subtable_subkey_from_key
    get_bs = h.private.get_bs[choice]
    set_signature_at = h.private.set_signature_at
    set_value_at = h.private.set_value_at
    get_value_at = h.private.get_value_at

    subtable, subkey = get_subtable_subkey(key)
    bucket, sig = get_bs(subkey)
    status, steps = store_new(h.hashtable, subtable, subkey, 2)
    assert steps == 1
    assert status == 1
    assert get_value_at(h.hashtable, subtable, bucket, 0) == 2

    status, result = overwrite(h.hashtable, subtable, subkey, 3)
    assert status - 128 == 1
    assert get_value_at(h.hashtable, subtable, bucket, 0) == result == 3

def test_overwrite_existing():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    key = 98104713013
    value = 1
    choice = 0

    store_new = h.private.store_new_ssk
    overwrite = h.private.overwrite_existing_ssk
    get_subtable_subkey = h.private.get_subtable_subkey_from_key
    get_bs = h.private.get_bs[choice]
    set_signature_at = h.private.set_signature_at
    set_value_at = h.private.set_value_at
    get_value_at = h.private.get_value_at

    subtable, subkey = get_subtable_subkey(key)
    bucket, sig = get_bs(subkey)
    status, steps = store_new(h.hashtable, subtable, subkey, 2)
    assert steps == 1
    assert status == 1
    assert get_value_at(h.hashtable, subtable, bucket, 0) == 2

    status, result = overwrite(h.hashtable, subtable, subkey, 3)
    assert status - 128 == 1
    assert get_value_at(h.hashtable, subtable, bucket, 0) == result == 3

    subtable, subkey = get_subtable_subkey(key+1)
    bucket, sig = get_bs(subkey)
    status, result = overwrite(h.hashtable, subtable, subkey, 3)
    assert status == 0
    assert get_value_at(h.hashtable, subtable, bucket, 0) == 0

def test_get_value():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    key = 98104713013
    value = 1
    choice = 0

    store_new = h.private.store_new_ssk
    get_subtable_subkey = h.private.get_subtable_subkey_from_key
    get_bs = h.private.get_bs[choice]
    get_value = h.private.get_value_from_st_sk

    subtable, subkey = get_subtable_subkey(key)
    bucket, sig = get_bs(subkey)
    status, steps = store_new(h.hashtable, subtable, subkey, 2)
    assert get_value(h.hashtable, subtable, subkey) == 2

def test_get_value_and_choice():
    h = build_hash(universe, n, subtables, bucketsize,
        hashfuncs, nvalues, update_value,
        aligned=True, nfingerprints=-1,
        maxwalk=500, shortcutbits=0)
    key = 98104713013
    value = 1
    choice = 0

    store_new = h.private.store_new_ssk
    get_subtable_subkey = h.private.get_subtable_subkey_from_key
    get_bs = h.private.get_bs[choice]
    get_value_and_choice = h.private.get_value_and_choice_from_st_sk

    subtable, subkey = get_subtable_subkey(key)
    bucket, sig = get_bs(subkey)
    status, steps = store_new(h.hashtable, subtable, subkey, 2)
    v, c = get_value_and_choice(h.hashtable, subtable, subkey)
    assert v == 2
    assert c == choice + 1